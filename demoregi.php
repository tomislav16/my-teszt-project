<?php

declare(strict_types=1);

require_once('vendor/autoload.php');

use IzzyPay\Exceptions\AuthenticationException;
use IzzyPay\Exceptions\InvalidAddressException;
use IzzyPay\Exceptions\InvalidCartItemException;
use IzzyPay\Exceptions\InvalidCustomerException;
use IzzyPay\Exceptions\InvalidCartException;
use IzzyPay\Exceptions\InvalidOtherException;
use IzzyPay\Exceptions\InvalidResponseException;
use IzzyPay\Exceptions\InvalidReturnDataException;
use IzzyPay\Exceptions\InvalidUrlsException;
use IzzyPay\Exceptions\PaymentServiceUnavailableException;
use IzzyPay\Exceptions\RequestException;
use IzzyPay\IzzyPay;
use IzzyPay\Models\Address;
use IzzyPay\Models\CartItem;
use IzzyPay\Models\Cart;
use IzzyPay\Models\LimitedCustomer;
use IzzyPay\Models\Customer;
use IzzyPay\Models\Other;
use IzzyPay\Models\Response\InitResponse;
use IzzyPay\Models\Response\StartResponse;
use IzzyPay\Models\StartOther;
use IzzyPay\Models\Urls;

$merchantCartId = '98';
$izzyPay = new IzzyPay('bitron', 'Nortib123.', 'https://test.izzypay.hu', 'plugin 1.0');
?>
<html>
<head>
<?php
function verifyCredential(IzzyPay $izzyPay): void
{
    try {
        $izzyPay->cred();
    } catch (RequestException|AuthenticationException $e) {
        var_dump($e->getMessage());
    }
}

function sendInit(IzzyPay $izzyPay, string $merchantCartId): ?InitResponse
{
    try {
        $cartItem = CartItem::create('Proba konyv','proba kateg', 'proba sub1', 'product', 5500, 1, 'ExampleManufacturer', '14', 'Some extra information.');
        $cart = Cart::create('HUF', 5500, [$cartItem]);
        $limitedCustomer = LimitedCustomer::create('guest', null, null, 'other');
        $other = Other::create('Google Chrome');
        return $izzyPay->init($merchantCartId, $cart, $limitedCustomer, $other);
    } catch (InvalidCustomerException|InvalidCartItemException|InvalidCartException|InvalidOtherException|InvalidResponseException|RequestException|JsonException|AuthenticationException|PaymentServiceUnavailableException $e) {
		print "error";
        var_dump($e->getMessage());
    }
    return null;
}

function sendStart(IzzyPay $izzyPay, string $merchantCartId, $token): ?StartResponse
{
    try {
        $cartItem = CartItem::create('name','category', 'subCategory', 'product', 6666.66, 1, 'manufacturer', 'merchantItemId', 'other');
        $cart = Cart::create('HUF', 6667.00, [$cartItem]);
        $address = Address::create('8888', 'city', 'street', 'houseNo', 'address1', 'address2', 'address3');
        $customer = Customer::create('merchant', 'merchantCustomerId', null,'other', 'name', 'surname', 'phone', 'email@emai.com', $address, $address);
        $other = StartOther::create('127.0.0.1', 'browser');
        $urls = Urls::create('https://ipn.com', 'https://checkout.com');
        return $izzyPay->start($token, $merchantCartId, $cart, $customer, $other, $urls);
    } catch (InvalidAddressException|InvalidCustomerException|InvalidCartItemException|InvalidCartException|InvalidOtherException|InvalidResponseException|RequestException|JsonException|InvalidUrlsException|AuthenticationException|PaymentServiceUnavailableException $e) {
        var_dump($e->getMessage());
    }
    return null;
}

function sendDeliveryCart(IzzyPay $izzyPay, string $merchantCartId): void
{
    try {
        $izzyPay->deliveryCart($merchantCartId);
    } catch (AuthenticationException|RequestException|JsonException $e) {
        var_dump($e->getMessage());
    }
}

function sendDeliveryItem(IzzyPay $izzyPay, string $merchantCartId, string $merchantItemId): void
{
    try {
        $izzyPay->deliveryItem($merchantCartId, $merchantItemId);
    } catch (AuthenticationException|RequestException|JsonException $e) {
        var_dump($e->getMessage());
    }
}

function sendReturnCart(IzzyPay $izzyPay, string $merchantCartId, DateTimeImmutable $returnDate): void
{
    try {
        $izzyPay->returnCart($merchantCartId, $returnDate);
    } catch (AuthenticationException|RequestException|JsonException $e) {
        var_dump($e->getMessage());
    }
}

function sendReturnItem(IzzyPay $izzyPay, string $merchantCartId, string $merchantItemId, DateTimeImmutable $returnDate, ?float $reducedValue = null): void
{
    try {
        $izzyPay->returnItem($merchantCartId, $merchantItemId, $returnDate, $reducedValue);
    } catch (AuthenticationException|RequestException|JsonException|InvalidReturnDataException $e) {
        var_dump($e->getMessage());
    }
}

// Used to check whether the configured credentials are correct.
// Not part of the normal flow, therefore doesn't need to be called before the init.
//verifyCredential($izzyPay);

$initResponse = sendInit($izzyPay, $merchantCartId);
print_r($initResponse);
if ($initResponse) {
    $jsUrl = $initResponse->getJsUrl();
	print '<script src ="'.$jsUrl.'"></script>';
    $token = $initResponse->getToken();
echo "\r\n<script>var token = '$token';</script>";    
}
?>
</head>
<body>
<h1>Start</h1>
<div id="izzypayDiv"></div>
<script>
var jsonData = `{
  "merchantId": "bitron",
  "merchantCartId": "98",
  "cart": {
    "items": [
      {
        "name": "Proba konyv",
        "category": "proba kateg",
        "subCategory": "proba sub1",
        "type": "product",
        "price": 5500,
        "quantity": 1,
        "manufacturer": "ExampleManufacturer",
        "merchantItemId": "14",
        "other": "Some extra information."
      }
    ],
    "totalValue": 5500,
    "currency": "HUF"
  },
  "customer": {
    "registered": "guest",
        "other": "other",
    "companyName":null
  },
  "other": {"browser": "Google Chrome"}
}`;

let enablerFn = function () { 
alert("enable");
}
let cartAndCustomerData = JSON.parse(jsonData);
console.log(cartAndCustomerData);
new IzzyPay('https://test.izzypay.hu', 'izzypayDiv',
token, cartAndCustomerData, enablerFn);

</script>

</body>
</html>